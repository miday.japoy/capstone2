const User = require("../models/user");
const Product = require("../models/product");
const bcrypt = require("bcrypt");
const auth = require("../auth");


// Registration Code
module.exports.userRegistration = (reqBody) => {

	let newUser = new User ({
		firstName: reqBody.firstName,
		lastName: reqBody.lastName,
		email: reqBody.email,
		mobileNo: reqBody.mobileNo,
		password: bcrypt.hashSync(reqBody.password, 10)
	})

	return User.find().then(results => {
		
		const email = newUser.email;
		const result = results.find(e => e.email === email);

		if(result === undefined){ 

			return newUser.save().then((user, error) => {

				if(error) {
					return ("Registration failed.")
				} else {
					return ("Registered.")
				}
			})

		} else { 
			return `Account already exists.`

		}

	})

	
}


// Login Code
module.exports.loginUser = (reqBody) => {

	return User.findOne({email: reqBody.email}).then(result => {

		if(result == null) {
			return `Account does not exist.`
		} else {

			const isPasswordCorrect = bcrypt.compareSync(reqBody.password, result.password)

			if (isPasswordCorrect == true && result.isAdmin == true) {
				
				return [`Welcome admin.`, { access: auth.createAccessToken(result)}]
			} else if(isPasswordCorrect == true && result.isAdmin == false) {
				return [`Welcome user.`, { access: auth.createAccessToken(result)}]
			}
			else {
				return `Wrong credentials.`
			}
		}
	})
}


//Setting user to admin code

module.exports.changeToAdmin = async (reqParams, data) => {	

		if(data.isAdmin){

			return User.findById({_id: reqParams.userId}).then((result) => {

				if(result == null) {
					return `User does not exist.`
				} else {
					result.isAdmin = true;
					return result.save().then(saveResult => {
						return `${result.firstName} ${result.lastName} is now an admin.`
					})
			}

		}) 
	
	} else { 

		return `Access denied.`
	}
			
}


