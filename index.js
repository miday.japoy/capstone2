const express = require('express');
const mongoose = require('mongoose');
const cors = require('cors');

const userRoutes = require("./routes/user");
const productRoutes = require("./routes/product");
const orderRoutes = require("./routes/order");


const port = process.env.PORT || 4000;

const app = express();

app.use(express.json());
app.use(express.urlencoded({extended: true}));
app.use(cors());


mongoose.connect('mongodb+srv://admin:admin@zuitt-bootcamp.nteyu.mongodb.net/eCommerce?retryWrites=true&w=majority',
	{
		useNewUrlParser: true,
		useUnifiedTopology: true

	}

)

let db = mongoose.connection;

		db.on('error', () => console.error.bind(console, "Connection Error."));
		db.once('open', () => console.log("Connected to MongoDB."))











app.use("/user", userRoutes, orderRoutes);
app.use("/product", productRoutes);


app.listen(port, () => console.log(`Local Host running at port:${port}`))