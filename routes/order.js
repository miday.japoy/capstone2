const express = require("express");
const router = express.Router();
const orderController = require("../controller/order");
const auth = require("../auth");



// Creating Order
router.post("/myOrder", auth.verify, (req, res) => {
	const userData = auth.decode(req.headers.authorization)

	orderController.createOrder(userData, req.body).then(resultFromController => res.send(resultFromController));
})

// Checking out order
router.post("/:orderId/checkout", auth. verify, (req,res) => {
	const userData = auth.decode(req.headers.authorization)

	if(userData.isAdmin) {
		return `Access Denied.`
	} else {
		let data = {
			userId: userData.id,
			orderId: req.params.orderId
		}

		orderController.checkoutOrder(data).then(resultFromController => res.send(resultFromController));
	}
})


// retrieving all order

router.get("/orders", auth.verify, (req, res) => {
	const userData = auth.decode(req.headers.authorization)

	orderController.retrieveAllOrders(userData).then(resultFromController => res.send(resultFromController));
})


// retrieving all users order

router.get("/myOrders", auth.verify, (req, res) => {
	const userData = auth.decode(req.headers.authorization)

	orderController.retrieveMyOrders(userData).then(resultFromController => res.send(resultFromController));
})




















module.exports = router;