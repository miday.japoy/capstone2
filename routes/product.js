const express = require("express");
const router = express.Router();
const productController = require("../controller/product");
const auth = require("../auth");


// Add Product(admin only)

router.post("/createNew", auth.verify, (req, res) => {
	const userData = auth.decode(req.headers.authorization)

	productController.addProduct(userData, req.body).then(resultFromController => res.send(resultFromController))

})


// Retrieving all Products

router.get("/", (req, res) => {

	productController.retrieveAllProduct().then(resultFromController => res.send(resultFromController));
})


// Retrieve single Product

router.get("/:productId", (req, res) => {

	productController.retrieveOneProduct(req.params).then(resultFromController => res.send(resultFromController));
})


// Update Product
router.put("/:productId/update", auth.verify, (req, res) => {

	const userData = auth.decode(req.headers.authorization)

	productController.updateProduct(userData, req.params, req.body).then(resultFromController => res.send(resultFromController));
})


// Archive Product

router.put("/:productId/archive", auth.verify, (req, res) => {
	
	const userData = auth.decode(req.headers.authorization)

	productController.archiveProduct(userData, req.params).then(resultFromController => res.send(resultFromController));
})







module.exports = router;